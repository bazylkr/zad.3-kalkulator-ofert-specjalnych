package src.main;

public class Klient {
	
	private float rabat;
	
	private String imie;
	
	Klient(float _rabat, String _imie){
		rabat = _rabat;
		imie = _imie;
	}
	
	public float getRabat(){
		return rabat;
	}
	
	public String getImie(){
		return imie;
	}
}
