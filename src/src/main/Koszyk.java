package src.main;

import java.util.ArrayList;
import java.util.List;

public class Koszyk {
	
	private List<Produkt> produkty;
	
	Koszyk(){
		produkty = new ArrayList<Produkt>();
	}
	
	public void dodajProdukt(Produkt produkt){
		produkty.add(produkt);
	}
	
	public void pokazProdukty(){
		
	}

	public List<Produkt> getProdukty() {
		return produkty;
	}

}
