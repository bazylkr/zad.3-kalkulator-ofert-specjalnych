package src.main;

public class Produkt {
	
	private float promocja;
	
	private float cena;
	
	private String nazwa;
	
	private int ofertaSpecjalna;
	
	
	Produkt(float _promocja, float _cena, String _nazwa, int _ofertaSpecjalna){
		promocja = _promocja;
		cena = _cena;
		nazwa = _nazwa;
		ofertaSpecjalna = _ofertaSpecjalna;
	}


	public float getPromocja() {
		return promocja;
	}


	public float getCena() {
		return cena;
	}


	public String getNazwa() {
		return nazwa;
	}


	public int getOfertaSpecjalna() {
		return ofertaSpecjalna;
	}
	
	public float policzOstatecznaCene(Klient klient, int ileProduktow){
		float ostatecznaCena = cena;
		ostatecznaCena -= ostatecznaCena*(promocja/100);
		ostatecznaCena -= ostatecznaCena*(klient.getRabat()/100);
		if(ofertaSpecjalna == 1){
			if(ileProduktow > 10){
				ostatecznaCena -= ostatecznaCena*0.5;
			}
		}
		else if(ofertaSpecjalna == 2){
			if(klient.getRabat() == 0){
				ostatecznaCena -= ostatecznaCena*0.1;
			}
		}
		
		return ostatecznaCena;
	}
}
